package hr.zdelarec.collegeassistant.main.view


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import hr.zdelarec.collegeassistant.R
import hr.zdelarec.collegeassistant.main.presenter.MainPresenterImpl
import hr.zdelarec.collegeassistant.utils.CoursesListAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), MainView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val toolbar = findViewById<Toolbar>(R.id.courses_toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.title = resources.getString(R.string.courses)
        courseInfo.visibility = View.GONE

        val mainPresenter = MainPresenterImpl(this)
        mainPresenter.getData()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.add_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        return when (item.itemId) {
            R.id.add -> {
                Toast.makeText(this,"Add", Toast.LENGTH_SHORT).show()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun showInfo() {
        courseInfo.visibility = View.VISIBLE
    }

    override fun showList(list: List<String>) {
        courseInfo.visibility = View.GONE
        courseList.layoutManager = LinearLayoutManager(this)
        courseList.adapter = CoursesListAdapter(list)
    }
}
