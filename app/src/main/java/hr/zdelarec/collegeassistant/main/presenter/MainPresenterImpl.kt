package hr.zdelarec.collegeassistant.main.presenter

import hr.zdelarec.collegeassistant.main.model.MainInteractor
import hr.zdelarec.collegeassistant.main.model.MainInteractorImpl
import hr.zdelarec.collegeassistant.main.view.MainView

class MainPresenterImpl(private var mainView: MainView) : MainPresenter {

    private var mainInteractor: MainInteractor = MainInteractorImpl()

    override fun getData() {
        val list = mainInteractor.generateMockData()

        if (list.isEmpty()) {
            mainView.showInfo()
        } else {
            mainView.showList(list)
        }
    }
}