package hr.zdelarec.collegeassistant.main.presenter

interface MainPresenter {
    fun getData()
}