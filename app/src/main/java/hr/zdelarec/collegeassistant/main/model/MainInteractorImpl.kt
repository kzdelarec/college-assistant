package hr.zdelarec.collegeassistant.main.model

class MainInteractorImpl : MainInteractor {

    override fun generateMockData(): List<String> {
        return listOf(
            "Item 1",
            "Item 2",
            "Item 3",
            "Item 4",
            "Item 5",
            "Item 6",
            "Item 7",
            "Item 8",
            "Item 9",
            "Item 10"
        )
    }
}