package hr.zdelarec.collegeassistant.main.model

interface MainInteractor {
    fun generateMockData() : List<String>
}