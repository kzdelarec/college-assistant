package hr.zdelarec.collegeassistant.main.view

interface MainView {
    fun showInfo()
    fun showList(list: List<String>)
}