package hr.zdelarec.collegeassistant.splash.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import hr.zdelarec.collegeassistant.main.view.MainActivity
import hr.zdelarec.collegeassistant.R
import hr.zdelarec.collegeassistant.splash.presenter.SplashScreenPresenterImpl
import id.voela.actrans.AcTrans
import kotlinx.android.synthetic.main.activity_splash_screen.*

class SplashScreenActivity : AppCompatActivity(),
    SplashScreenView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        val splashScreenPresenter =
            SplashScreenPresenterImpl(this)
        splashScreenPresenter.start()
    }

    override fun showLogo() {
        YoYo.with(Techniques.FadeInDown).duration(1500).onEnd {
            goToMain()
        }.playOn(splash)
    }

    override fun goToMain() {
        startActivity(Intent(this, MainActivity::class.java))
        AcTrans.Builder(this).performSlideToLeft()
        finish()
    }
}
