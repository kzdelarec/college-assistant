package hr.zdelarec.collegeassistant.splash.presenter

import hr.zdelarec.collegeassistant.splash.view.SplashScreenView

class SplashScreenPresenterImpl(private val splashScreenView: SplashScreenView) :
    SplashScreenPresenter {

    fun start(){
        splashScreenView.showLogo()
    }
}