package hr.zdelarec.collegeassistant.splash.view

interface SplashScreenView {
    fun showLogo()
    fun goToMain()
}