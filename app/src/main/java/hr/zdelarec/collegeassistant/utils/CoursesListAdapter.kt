package hr.zdelarec.collegeassistant.utils

import android.view.LayoutInflater
import android.view.TextureView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import hr.zdelarec.collegeassistant.R

class CoursesListAdapter(private val list: List<String>) : RecyclerView.Adapter<CoursesListAdapter.CourseItemHolder>(){

    class CourseItemHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val colorView: View = itemView.findViewById(R.id.courseColorView)
        val nameTextView: TextView = itemView.findViewById(R.id.courseName)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CourseItemHolder {
        val layoutInflater: LayoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.course_list_item, parent, false)
        return CourseItemHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: CourseItemHolder, position: Int) {
        holder.nameTextView.text = list[position]
    }
}